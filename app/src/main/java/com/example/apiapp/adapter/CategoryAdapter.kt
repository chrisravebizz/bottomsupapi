package com.example.apiapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.apiapp.databinding.ItemListBinding
import com.example.apiapp.model.response.CategoryDTO

class CategoryAdapter(private val navDrink: (String) -> Unit) :
    RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    private var category = mutableListOf<CategoryDTO.CategoryItem>()

    /* Set up View Holder */
    // This method inflates card layout items for Recycler View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        CategoryViewHolder.getInstance(parent)

    // This method sets the data to specific views of card items.
    // It also handles methods related to clicks on items of Recycler view.
    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        val categoryData = category[position]
        holder.bindCategory(categoryData)
        holder.returnDrink().setOnClickListener {
            navDrink(categoryData.strCategory)
        }
    }

    // This method returns the length of the RecyclerView.
    override fun getItemCount(): Int {
        return category.size
    }

    // === Custom Function to pass object[it] ===
    // viewModel gets category and assigns data to category variable
    fun loadCategory(item: List<CategoryDTO.CategoryItem>) {
        this.category = item.toMutableList()
        notifyDataSetChanged()
    }

    class CategoryViewHolder(
        private val binding: ItemListBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        // This function gets the data from category member variable / position and
        // passes data as an argument to this function
        fun bindCategory(category: CategoryDTO.CategoryItem) {
            binding.tvItemText.text = category.strCategory
        }

        fun returnDrink(): TextView {
            return binding.tvItemText
        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemListBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { CategoryViewHolder(it) }
        }
    }
}