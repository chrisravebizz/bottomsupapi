package com.example.apiapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.example.apiapp.databinding.ItemDrinkDetailBinding
import com.example.apiapp.model.response.DrinkDetailsDTO
import com.squareup.picasso.Picasso

class DrinkDetailAdapter : RecyclerView.Adapter<DrinkDetailAdapter.DrinkDetailViewHolder>() {

    private var drink = mutableListOf<DrinkDetailsDTO.Drink>()

    /* Set up View Holder */
    // This method inflates card layout items for Recycler View
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        DrinkDetailViewHolder.getInstance(parent)

    // This method sets the data to specific views of card items.
    // It also handles methods related to clicks on items of Recycler view.
    override fun onBindViewHolder(holder: DrinkDetailViewHolder, position: Int) {
        val drinkDetailData = drink[position]
        holder.bindDrinkDetail(drinkDetailData)
    }

    // This method returns the length of the RecyclerView.
    override fun getItemCount(): Int {
        return drink.size
    }

    // === Custom Function to pass object[it] ===
    // viewModel gets drink and assigns data to drink variable
    fun loadDrinkDetail(item: List<DrinkDetailsDTO.Drink>) {
        this.drink = item.toMutableList()
        notifyDataSetChanged()
    }

    class DrinkDetailViewHolder(
        private val binding: ItemDrinkDetailBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        // This function gets the data from drink member variable / position and
        // passes data as an argument to this function
        fun bindDrinkDetail(drink: DrinkDetailsDTO.Drink) {
            binding.tvDrinkDetail.text = drink.strDrink
            binding.tvDrinkCategory.text = drink.strCategory
            Picasso.get().load(drink.strDrinkThumb).into(binding.drinkThumb)
        }

//        fun returnDrinkDetail(): ImageView {
//            return binding.drinkThumb
//        }

        companion object {
            fun getInstance(parent: ViewGroup) = ItemDrinkDetailBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            ).let { DrinkDetailViewHolder(it) }
        }
    }
}