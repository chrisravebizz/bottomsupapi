package com.example.apiapp.model

import android.util.Log
import com.example.apiapp.model.remote.BottomsUpService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.math.log

object BottomsUpRepo {

    // Get instance of our service
    private val bottomsUpService by lazy { BottomsUpService.getInstance() }

    suspend fun getCategories() = withContext(Dispatchers.IO) {
        bottomsUpService.getCategories()
    }

    suspend fun getDrinksInCategory(category: String) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinksInCategory(category).drinks
    }

    suspend fun getDrinkDetails(drinkId: Int) = withContext(Dispatchers.IO) {
        bottomsUpService.getDrinkDetails(drinkId).drinks
    }

}