package com.example.apiapp.model.response

import com.google.gson.annotations.SerializedName

data class CategoryDTO(
    // Parser for GSON library to match the property name
    @SerializedName("drinks")
    val categoryItems: List<CategoryItem>
) {
    data class CategoryItem(
        val strCategory: String
    )
}