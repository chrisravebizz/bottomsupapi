package com.example.apiapp.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import com.example.apiapp.R
import com.example.apiapp.viewmodel.CategoryViewModel

class MainActivity : AppCompatActivity() {
    
    private val categoryViewModel by viewModels<CategoryViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        categoryViewModel
    }
}