package com.example.apiapp.view.category

import com.example.apiapp.model.response.CategoryDTO

class CategoryState(
    val isLoading: Boolean = false,
    val categories: List<CategoryDTO.CategoryItem> = emptyList()
)