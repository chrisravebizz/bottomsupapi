package com.example.apiapp.view.drink

import com.example.apiapp.model.response.CategoryDrinksDTO

class DrinkState(
    val isLoading: Boolean = false,
    val drinks: List<CategoryDrinksDTO.Drink> = emptyList()
)