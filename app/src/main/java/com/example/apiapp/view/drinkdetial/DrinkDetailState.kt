package com.example.apiapp.view.drinkdetial

import com.example.apiapp.model.response.DrinkDetailsDTO

class DrinkDetailState(
    val isLoading: Boolean = false,
    val drinkDetails: List<DrinkDetailsDTO.Drink> = emptyList()
)