package com.example.apiapp.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.apiapp.model.BottomsUpRepo
import com.example.apiapp.view.drink.DrinkState
import kotlinx.coroutines.launch

class DrinkViewModel() : ViewModel() {

    private val repo by lazy { BottomsUpRepo }
    private val _state = MutableLiveData(DrinkState(isLoading = true))
    val state: LiveData<DrinkState> get() = _state

    fun getDrinksList(category: String) {
        viewModelScope.launch {
            val drinkDTO = repo.getDrinksInCategory(category)
            _state.value = DrinkState(drinks = drinkDTO)
        }
    }
}