//package com.example.apiapp.viewmodel
//
//import androidx.lifecycle.ViewModel
//import androidx.lifecycle.ViewModelProvider
//
//class DrinkViewModelFactory(val drink : String) : ViewModelProvider.Factory {
//    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
//        return modelClass.getConstructor(String::class.java)
//            .newInstance(drink)
//    }
//}